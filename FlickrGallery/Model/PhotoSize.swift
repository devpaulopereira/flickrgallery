//
//  PhotoSize.swift
//  FlickrGallery
//
//  Created by Paulo Pereira on 04/03/19.
//  Copyright © 2019 Paulo Pereira. All rights reserved.
//

import Foundation

public struct PhotoSize: Decodable {

    public let canblog: Int
    public let canprint: Int
    public let candownload: Int
    public let size: [Size]

    public struct Size: Decodable {
        public let label: String
        public let source: String
        public let url: String
        public let media: String
    }
}
