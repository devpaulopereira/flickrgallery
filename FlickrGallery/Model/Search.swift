//
//  Search.swift
//  FlickrGallery
//
//  Created by Paulo Pereira on 03/03/19.
//  Copyright © 2019 Paulo Pereira. All rights reserved.
//

import Foundation

public struct Search: Decodable {

    public let page: Int
    public let pages: Int
    public let perpage: Int
    public let total: String
    public let photo: [Photo]

    public struct Photo: Decodable {
        public let id: String
        public let owner: String
        public let secret: String
        public let server: String
        public let farm: Int
        public let title: String
        public let ispublic: Int
        public let isfriend: Int
        public let isfamily: Int
    }
}
