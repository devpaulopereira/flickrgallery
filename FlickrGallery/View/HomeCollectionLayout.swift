//
//  HomeCollectionLayout.swift
//  FlickrGallery
//
//  Created by Paulo Pereira on 03/03/19.
//  Copyright © 2019 Paulo Pereira. All rights reserved.
//

import Foundation
import UIKit

struct Device {
    enum sizeType: String {
        case small
        case medium
        case large
    }
    public let screenSize: CGRect = UIScreen.main.bounds
}

class HomeCollectionLayout: UICollectionViewFlowLayout {

    override init() {
        super.init()

        minimumLineSpacing = 16
        minimumInteritemSpacing = minimumLineSpacing
        sectionInset = UIEdgeInsets(top: minimumLineSpacing,
                                    left: minimumLineSpacing,
                                    bottom: minimumLineSpacing,
                                    right: minimumLineSpacing)
    }

    private var itemsOnRow: CGFloat {
        let size = UIScreen.rotationSize()
        switch size {
        case .small:
            return 2
        case .medium:
            return 4
        case .large:
            return 6
        }
    }

    func size(collectionView: UICollectionView) -> CGSize {
        let width = sectionInset.left + sectionInset.right + (minimumInteritemSpacing * CGFloat(itemsOnRow - 1))
        let size = CGFloat((collectionView.bounds.width - width) / itemsOnRow)
        return CGSize(width: size, height: 300)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

extension UIScreen {

    static func rotationSize() -> Device.sizeType {
        if UIScreen.main.bounds.width > 1111 {
            return .large
        }
        if UIScreen.main.bounds.width > 665 {
            return .medium
        }
        return .small
    }
}
