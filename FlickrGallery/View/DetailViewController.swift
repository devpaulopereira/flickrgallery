//
//  DetailViewController.swift
//  FlickrGallery
//
//  Created by Paulo Pereira on 04/03/19.
//  Copyright © 2019 Paulo Pereira. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class DetailViewController: UIViewController {

    var photo: PhotoSize.Size? {
        didSet {
            imageView.imageWithUrl(urlImage: photo?.source)
        }
    }

    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.masksToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor  = .black
        title = "Detail"
        setupView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    private func setupView() {
        view.addSubview(imageView)

        if #available(iOS 11.0, *) {
        NSLayoutConstraint.activate([imageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
                                     imageView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
                                     imageView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
                                     imageView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
            ])
        } else {
        NSLayoutConstraint.activate([imageView.topAnchor.constraint(equalTo: view.topAnchor),
                                     imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                                     imageView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                                     imageView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ])
        }
    }

}
