//
//  ViewController.swift
//  FlickrGallery
//
//  Created by Paulo Pereira on 03/03/19.
//  Copyright © 2019 Paulo Pereira. All rights reserved.
//

import UIKit

class HomeViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    private let searchViewModel = SearchViewModel()
    private let imageViewModel = ImageSizesViewModel()
    var tags: [String] = []

    lazy var searchBar: UISearchBar = {
        let searchbar = UISearchBar(frame: .zero)
        searchbar.placeholder = "Search by Tags"
        searchbar.delegate = self
        return searchbar
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        searchViewModel.completion = { [weak self] state in
            switch state {
            case .failure(_):
                self?.searchBar.isLoading = false
                self?.alert(title: "Error", message: "Sorry, we found an error :(")
            case .success:
                self?.collectionView.reloadData()
                self?.searchBar.isLoading = false
            }
        }

        imageViewModel.completion = { [weak self] state in
            switch state {
            case .failure(_):
                self?.searchBar.isLoading = false
                self?.alert(title: "Error", message: "Sorry, we found an error :(")
            case .success:
                let detailViewController = DetailViewController()
                detailViewController.photo = self?.imageViewModel.imageDetail
                self?.navigationController?.pushViewController(detailViewController, animated: true)
            }
        }
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    private func setupView() {
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        collectionView.showsVerticalScrollIndicator = false
        collectionView.backgroundColor = UIColor(red: 19, green: 19, blue: 19)
        navigationItem.titleView = searchBar
        searchBar.clearStyle()
    }

    override init(collectionViewLayout layout: UICollectionViewLayout) {
        super.init(collectionViewLayout: layout)
        collectionView.register(ImageViewCell.self,
                                forCellWithReuseIdentifier: ImageViewCell.description())
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return searchViewModel.images.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let imageViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageViewCell.description(), for: indexPath) as! ImageViewCell
        imageViewCell.photo = searchViewModel.images[indexPath.row]
        return imageViewCell
    }

    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let size = (collectionViewLayout as? HomeCollectionLayout)?
            .size(collectionView: collectionView) else { return collectionView.contentSize }
        return size
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let imageIdentifier = self.searchViewModel.images[indexPath.row].imageIdentifier {
            imageViewModel.imageSizesByIdentifier(photoIdentifier: imageIdentifier)
        }
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        collectionView.collectionViewLayout.invalidateLayout()
    }

    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        nextPage(scrollView)
    }

    private func scrollToUpdate(_ heigth: CGFloat) -> CGFloat {
        return heigth * 0.40
    }

    private func nextPage(_ scrollView: UIScrollView) {
        if scrollToUpdate(scrollView.contentSize.height) < scrollView.contentOffset.y {
                searchViewModel.searchByTag(tags: tags)
        }
    }

    private func alert(title: String, message: String) {
        let alert = UIAlertController(title:  title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

extension HomeViewController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let text = searchBar.text, !text.isEmpty {
            tags = []
            searchBar.isLoading = true
            let spaces = CharacterSet.whitespacesAndNewlines.union(.punctuationCharacters)
            let words = text.components(separatedBy: spaces)
            words.forEach({ tags.append($0) })
            searchViewModel.searchByTag(tags: tags)
        }
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
    }

    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
}

extension UISearchBar {
    private var textField: UITextField? {
        let subViews = self.subviews.flatMap { $0.subviews }
        return (subViews.filter { $0 is UITextField }).first as? UITextField
    }

    private var activityIndicator: UIActivityIndicatorView? {
        return textField?.leftView?.subviews.compactMap { $0 as? UIActivityIndicatorView }.first
    }

    func clearStyle() {
        textField?.leftViewMode = .unlessEditing
        self.setImage(UIImage(), for: .search, state: .normal)
    }

    var isLoading: Bool {
        get {
            return activityIndicator != nil
        } set {
            if newValue {
                if activityIndicator == nil {
                    let activityIndicator = UIActivityIndicatorView(style: .gray)
                    activityIndicator.startAnimating()
                    activityIndicator.backgroundColor = UIColor.clear
                    textField?.leftView?.addSubview(activityIndicator)
                    let leftViewSize = textField?.leftView?.frame.size ?? CGSize.zero
                    activityIndicator.center = CGPoint(x: leftViewSize.width/2, y: leftViewSize.height/2)
                }
            } else {
                activityIndicator?.removeFromSuperview()
            }
        }
    }
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
}
