//
//  ImageViewCell.swift
//  FlickrGallery
//
//  Created by Paulo Pereira on 03/03/19.
//  Copyright © 2019 Paulo Pereira. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class ImageViewCell: UICollectionViewCell {

    var cell: AnyClass = ImageViewCell.self

    var photo: PhotoUI? {
        didSet {
            setupView()
        }
    }

    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.masksToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
    }

    private func setupView() {
        addSubview(imageView)
        formatConstrantsByVisualFormat(views: ["imageView": imageView],
                                       with: ["H:|[imageView]|",
                                              "V:|[imageView]|"])
        imageView.imageWithUrl(urlImage: photo?.imageURL)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

extension UIImageView {
        func imageWithUrl(urlImage: URL, placeholder: UIImage? = nil) {

        let referenceSize: CGSize = CGSize(width: UIScreen.main.bounds.width * 0.3,
                                           height: UIScreen.main.bounds.height * 0.3)
        let processor = ResizingImageProcessor(referenceSize: referenceSize, mode: .aspectFill)
        kf.setImage(with: urlImage,
                    placeholder: placeholder,
                    options: [.processor(processor),
                              .scaleFactor(UIScreen.main.scale),
                              .transition(.fade(0.3))])
    }

    func imageWithUrl(urlImage: String?, placeholder: UIImage? = nil) {
        if let urlImage = urlImage, let urlLink = URL(string: urlImage) {
            imageWithUrl(urlImage: urlLink, placeholder: placeholder)
            return
        }
        self.image = placeholder
    }
}
