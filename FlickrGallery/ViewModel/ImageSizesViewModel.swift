//
//  ImageSizesViewModel.swift
//  FlickrGallery
//
//  Created by Paulo Pereira on 04/03/19.
//  Copyright © 2019 Paulo Pereira. All rights reserved.
//

import Foundation

public struct PhotoSizesUI {
    let label: String?
    let width: Any?
    let height: Any?
    let source: String?
    let url: String?
    let media: String?
}

class ImageSizesViewModel {
    private let searchBusiness: FlickrSearchBusiness = FlickrSearchBusiness()
    var completion: (ImageSizeState) -> Void = { _ in }
    var imageSize: PhotoSizesUI?
    var imageDetail: PhotoSize.Size?

    enum ImageSizeState {
        case failure(Error)
        case success()
    }

     func imageSizesByIdentifier(photoIdentifier: String) {
        searchBusiness.imagesSizesByIdentifier(photoIdentifier: photoIdentifier, callback: { [weak self] result in
            if let sizes = result.data {
                self?.imageDetail = sizes.size.filter { ($0.label == "Large") }.first
                self?.completion(.success())
            } else {
                self?.completion(.failure(result.error ?? NSError(domain: "", code: -10, userInfo: nil)))
            }
        })
    }
}
