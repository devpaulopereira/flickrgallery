//
//  SearchViewModel.swift
//  FlickrGallery
//
//  Created by Paulo Pereira on 03/03/19.
//  Copyright © 2019 Paulo Pereira. All rights reserved.
//

import Foundation

public struct PhotoUI {
    let secret: String?
    let server: String?
    let imageIdentifier: String?
    let title: String?
    let imageURL: String?
 }

class SearchViewModel {

    private let searchBusiness: FlickrSearchBusiness = FlickrSearchBusiness()
    var completion: (SearchState) -> Void = { _ in }
    var images: [PhotoUI] = []
    private var isPageLocked: Bool = false
    var pageSearch: Int = 1
    var pageUpcoming: Int = 1
    private var lastQuerySearch: [String] = []

    enum SearchState {
        case failure(Error)
        case success
    }

    func searchByTag(tags: [String]) {
        if isPageLocked {
            return
        }
        isPageLocked = true
        if lastQuerySearch != tags {
            lastQuerySearch = tags
            pageSearch = 1
        }
        searchBusiness.searchImagesByTag(tags: tags, pageNum: pageSearch, callback: { [weak self] result in
            self?.isPageLocked = false
            if let search = result.data {
                if self?.pageSearch == 1 {
                    self?.images = []
                }
                self?.pageSearch += 1
                search.photo.forEach({ photo in
                    let photoUI = PhotoUI(secret: photo.secret, server: photo.secret, imageIdentifier: photo.id, title: photo.title, imageURL: self?.formatImageURL(photo: photo))
                    self?.images.append(photoUI)
                })
                self?.completion(.success)
            } else {
                self?.completion(.failure(result.error ?? NSError(domain: "", code: -10, userInfo: nil)))
            }
        })
    }

    private func formatImageURL(photo: Search.Photo) -> String {
        let baseURL = "https://farm5.static.flickr.com/\(String(describing: photo.server))/\(String(describing: photo.id))_\(String(describing: photo.secret)).jpg"
        return baseURL
    }
}
