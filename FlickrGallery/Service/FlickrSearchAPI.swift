//
//  FlickrSearchAPI.swift
//  FlickrGallery
//
//  Created by Paulo Pereira on 03/03/19.
//  Copyright © 2019 Paulo Pereira. All rights reserved.
//

import Foundation
import Moya

enum FlickrSearchAPI {
    case images(tags: [String], pageNum: Int)
    case imageSizes(photoIdentifier: String)
}
extension FlickrSearchAPI: TargetType {
    private var apiKey: String { return "f9cc014fa76b098f9e82f1c288379ea1" }

    var baseURL: URL { return URL(string: "https://api.flickr.com")! }

    var path: String {
        switch self {
        case .images:
            return "services/rest/"
        case .imageSizes:
            return "services/rest/"
        }
    }

    var method: Moya.Method {
        switch self {
        case .images:
            return .get
        case .imageSizes:
            return .get
        }
    }

    var sampleData: Data {
        return Data()
    }

    var task: Task {
        switch self {
        case .images(let tags, let pageNum):
            return .requestParameters(
                parameters: ["method": "flickr.photos.search",
                "api_key": apiKey,
                "tags": (tags as NSArray).componentsJoined(by: ","),
                "page": pageNum,
                "format": "json",
                "nojsoncallback": 1],
                encoding: URLEncoding.default)
        case .imageSizes(let photoIdentifier):
            return .requestParameters(
                parameters: ["method": "flickr.photos.getSizes",
                "api_key": apiKey,
                "photo_id": photoIdentifier,
                "format": "json",
                "nojsoncallback": 1],
                encoding: URLEncoding.default)
        }
    }

    var headers: [String : String]? {
        return nil
    }
}
