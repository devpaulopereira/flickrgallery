//
//  FlickrSearchBusiness.swift
//  FlickrGallery
//
//  Created by Paulo Pereira on 03/03/19.
//  Copyright © 2019 Paulo Pereira. All rights reserved.
//

import Foundation
import Moya

public class FlickrSearchBusiness {
    private let provider = MoyaProvider<FlickrSearchAPI>()

    public init() {}

    public func searchImagesByTag(tags: [String], pageNum: Int, callback: @escaping (Callback<Search>) -> Void) {
        provider.request(FlickrSearchAPI.images(tags: tags, pageNum: pageNum)) { result in
            switch result {
            case let .success(response):
                do {
                    let result = try response.map(Search.self, atKeyPath: "photos", using: JSONDecoder(), failsOnEmptyData: false)
                    callback(Callback(result: result))
                } catch let error {
                    callback(Callback(error: error))
                }
            case let .failure(error):
                    callback(Callback(error: error))
            }
        }
    }

    public func imagesSizesByIdentifier(photoIdentifier: String, callback: @escaping (Callback<PhotoSize>) -> Void) {
        provider.request(FlickrSearchAPI.imageSizes(photoIdentifier: photoIdentifier)) { result in
            switch result {
            case let .success(response):
                do {
                    let result = try response.map(PhotoSize.self, atKeyPath: "sizes", using: JSONDecoder(), failsOnEmptyData: false)
                    callback(Callback(result: result))
                } catch let error {
                    callback(Callback(error: error))
                }
            case let .failure(error):
                    callback(Callback(error: error))
            }
        }
    }
}
