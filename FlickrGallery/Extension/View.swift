//
//  View.swift
//  FlickrGallery
//
//  Created by Developer on 03/03/19.
//  Copyright © 2019 Paulo Pereira. All rights reserved.
//

import Foundation
import UIKit

extension UIView {

    func formatConstrantsByVisualFormat(views: [String: UIView], with visualFormats: [String]) {
        views.map {($0.value)}.forEach { (view) in
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        visualFormats.forEach { (visualFormat) in
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: visualFormat,
                                                          options: NSLayoutConstraint.FormatOptions(),
                                                          metrics: nil,
                                                          views: views))
        }
    }

    func activateConstraints(_ constraints: [NSLayoutConstraint]) {
        translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(constraints)
    }

    var safeAreaTopAnchor: NSLayoutYAxisAnchor {
        get {
            if #available(iOS 11.0, *) {
                return safeAreaLayoutGuide.topAnchor
            }
            return topAnchor
        }
    }

    var safeAreaBottonAnchor: NSLayoutYAxisAnchor {
        get {
            if #available(iOS 11.0, *) {
                return safeAreaLayoutGuide.bottomAnchor
            }
            return bottomAnchor
        }
    }

    var safeAreaLeadingAnchor: NSLayoutXAxisAnchor {
        get {
            if #available(iOS 11.0, *) {
                return safeAreaLayoutGuide.leadingAnchor
            }
            return leadingAnchor
        }
    }

    var safeAreaTrailingAnchor: NSLayoutXAxisAnchor {
        get {
            if #available(iOS 11.0, *) {
                return safeAreaLayoutGuide.trailingAnchor
            }
            return trailingAnchor
        }
    }
}
