# FlickrGallery

iOS App for search photos on Flick by Tags

![](https://i.imgur.com/VdUxJEy.png)

## Installation

Use cocoapods

```bash
gem install cocoapods
```

## Usage

```pod install
```

## Libraries

| Library   | Type  
| ------------ | ------------
| Kingfisher  | Download and image cache  
| Moya  | Network  

**Yes, you can create this APP without the need for these libraries, but they make our lives much easier.**

Kingfisher help with image download and cache <3

Moya, supports API contracts

![](https://sd.keepcalm-o-matic.co.uk/i-w600/keep-calm-and-love-moya-2.jpg)

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

